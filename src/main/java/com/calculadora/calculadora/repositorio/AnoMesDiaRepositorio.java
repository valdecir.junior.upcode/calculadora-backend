package com.calculadora.calculadora.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.calculadora.calculadora.modelo.AnoMesDia;

@Repository
public interface AnoMesDiaRepositorio extends JpaRepository<AnoMesDia, Long> {

	@Transactional
	@Modifying
	@Query("DELETE  FROM AnoMesDia WHERE idUsuario = :id")
	void deleteAnoMesDiaByIDUsuario(@Param("id") long id);

	
	


}