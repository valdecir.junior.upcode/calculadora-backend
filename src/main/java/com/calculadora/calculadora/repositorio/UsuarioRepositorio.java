package com.calculadora.calculadora.repositorio;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.calculadora.calculadora.modelo.Usuario;


@Repository
public interface UsuarioRepositorio extends JpaRepository<Usuario, Long> {
	public Optional<Usuario> findByNome(String nome);
}