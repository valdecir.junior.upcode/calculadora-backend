package com.calculadora.calculadora.controle;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.calculadora.calculadora.modelo.AnoMesDia;
import com.calculadora.calculadora.modelo.Usuario;
import com.calculadora.calculadora.repositorio.AnoMesDiaRepositorio;
import com.calculadora.calculadora.repositorio.UsuarioRepositorio;

@RestController
public class AnoMesDiaControle {
	@Autowired
	private AnoMesDiaRepositorio _anoMesDiaRepository;

	@Autowired
	private UsuarioRepositorio _usuarioDiaRepository;

	@RequestMapping(value = "/anomesdia", method = RequestMethod.GET)
	public List<AnoMesDia> Get() {
		return _anoMesDiaRepository.findAll();
	}

	@RequestMapping(value = "/anomesdia/{id}", method = RequestMethod.GET)
	public ResponseEntity<AnoMesDia> GetById(@PathVariable(value = "id") long id) {
		Optional<AnoMesDia> anoMesDia = _anoMesDiaRepository.findById(id);
		if (anoMesDia.isPresent())
			return new ResponseEntity<AnoMesDia>(anoMesDia.get(), HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@SuppressWarnings("finally")
	@RequestMapping(value = "/anomesdia", method = RequestMethod.POST)
	public AnoMesDia Post(@Valid @RequestBody AnoMesDia anoMesDia) {

		try {
			Optional<Usuario> usuario = _usuarioDiaRepository.findById(anoMesDia.getIdUsuario());

			anoMesDia.setUsuarioOficial(usuario.get());

		} catch (Exception ex) {
			System.out.println(ex);
		} finally {
			return _anoMesDiaRepository.save(anoMesDia);
		}

	}

	@RequestMapping(value = "/anomesdia/{id}", method = RequestMethod.PUT)
	public ResponseEntity<AnoMesDia> Put(@PathVariable(value = "id") long id,
			@Valid @RequestBody AnoMesDia newAnoMesDia) {

		Optional<AnoMesDia> oldAnoMesDia = _anoMesDiaRepository.findById(id);
		if (oldAnoMesDia.isPresent()) {

			AnoMesDia anoMesDia = oldAnoMesDia.get();
			anoMesDia.setCalculo(newAnoMesDia.getCalculo());
			anoMesDia.setDescricao(newAnoMesDia.getDescricao());
			anoMesDia.setResultado(newAnoMesDia.getResultado());

			_anoMesDiaRepository.save(anoMesDia);
			return new ResponseEntity<AnoMesDia>(anoMesDia, HttpStatus.OK);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/anomesdia/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id) {
	
	
		 _anoMesDiaRepository.deleteAnoMesDiaByIDUsuario(id);
			
		 System.out.println("Entrou");
		 
			return new ResponseEntity<>(HttpStatus.OK);
		
		
	}
	
	
	
	
}