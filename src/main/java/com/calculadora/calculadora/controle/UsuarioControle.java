package com.calculadora.calculadora.controle;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.calculadora.calculadora.modelo.Usuario;
import com.calculadora.calculadora.modelo.ValidaUsuario;
import com.calculadora.calculadora.repositorio.UsuarioRepositorio;

@RestController
public class UsuarioControle {
	@Autowired
	private UsuarioRepositorio _usuarioRepository;

	@RequestMapping(value = "/usuario", method = RequestMethod.GET)
	public List<Usuario> Get() {
		return _usuarioRepository.findAll();
	}

	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.GET)
	public ResponseEntity<Usuario> GetById(@PathVariable(value = "id") long id) {
		Optional<Usuario> usuario = _usuarioRepository.findById(id);
		if (usuario.isPresent())
			return new ResponseEntity<Usuario>(usuario.get(), HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@GetMapping(value = "/usuario/nome/{nome}")
	public ResponseEntity<ValidaUsuario> GetByNome(@PathVariable(value = "nome") String nome) {
		Optional<Usuario> usuario = _usuarioRepository.findByNome(nome);
		ValidaUsuario validacao = new ValidaUsuario();
		if (usuario.isPresent())
			validacao.setOk(1);
		else
			validacao.setOk(0);
		
		return ResponseEntity.ok(validacao);
	}

	@RequestMapping(value = "/usuario", method = RequestMethod.POST)
	public Usuario Post(@Valid @RequestBody Usuario usuario) {
		return _usuarioRepository.save(usuario);
	}

	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Usuario> Put(@PathVariable(value = "id") long id, @Valid @RequestBody Usuario newUsuario) {
		Optional<Usuario> oldUsuario = _usuarioRepository.findById(id);
		if (oldUsuario.isPresent()) {
			Usuario usuario = oldUsuario.get();
			usuario.setNome(newUsuario.getNome());
	
			usuario.setSenha(newUsuario.getSenha());
			_usuarioRepository.save(usuario);
			return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/usuario/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id) {
		Optional<Usuario> usuario = _usuarioRepository.findById(id);
		if (usuario.isPresent()) {
			_usuarioRepository.delete(usuario.get());
			return new ResponseEntity<>(HttpStatus.OK);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}