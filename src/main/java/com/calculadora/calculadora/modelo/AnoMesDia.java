package com.calculadora.calculadora.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author upcod
 *
 */
@Entity
@Table(name = "AnoMesDia")
@Data
public class AnoMesDia implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(nullable = true)
	private long idUsuario;

	@Column(nullable = true)
	private String calculo;

	@Column(nullable = true)
	private String resultado;

	@Column(nullable = true)
	private String descricao;

	@ManyToOne

	private Usuario usuarioOficial;

	public Usuario getUsuarioOficial() {
		return usuarioOficial;
	}

	public void setUsuarioOficial(Usuario usuarioOficial) {
		this.usuarioOficial = usuarioOficial;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCalculo() {
		return calculo;
	}

	public void setCalculo(String calculo) {
		this.calculo = calculo;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
